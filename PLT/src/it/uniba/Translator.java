package it.uniba;

public class Translator {
	
	private Phrase phrase = null;
	
	public static final String NIL = TranslatorConstant.NIL;
	public static final String NAY = TranslatorConstant.NAY;
	public static final String YAY = TranslatorConstant.YAY;
	public static final String AY = TranslatorConstant.AY;
	
	public Translator(String inputPhrase) throws PigLatinException{
		this.phrase = new Phrase(inputPhrase);
		this.phrase.translate();
	}
	
	public String getPhrase() throws PigLatinException{
		return this.phrase.getPhrase();
	}
	
	public static void main(String[] args) {
		String inputPhrase = "!";
		
		Translator translator;
		try {
			translator = new Translator(inputPhrase);
			translator.getPhrase();
		} catch (PigLatinException e) {
			e.printStackTrace();
		}
		
		
	}

}
