package it.uniba;

public class TranslatorConstant {
	public static final String NIL = "nil";
	
	public static final String NAY = "nay";
	public static final String YAY = "yay";
	public static final String AY = "ay";
	
	private TranslatorConstant() {}
}
