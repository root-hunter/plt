package it.uniba;

public class PigLatinException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public PigLatinException(String errorMessage) {
		super(errorMessage);
	}
}
