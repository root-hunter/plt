package it.uniba;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Word {
	protected String content = null;
	protected boolean compositeWord = false;
	protected boolean finalPunctation = false;
	
	protected boolean upperCaseWord = false;
	protected boolean titleCaseWord = false;
	
	protected String originalString = null;
	protected int originalLength = 0;
	
	
	public Word(String word) throws PigLatinException{
		content = word;
		
		handlerException();
		
		initOriginal();
		checkCaseProprieties();		
		checkWordExtraProprities();		
	}
	
	public void handlerException() throws PigLatinException{
		if (content.length() == 1 && isPunctuation(String.valueOf(content.charAt(0)))) {
			throw new PigLatinException("Formato stringa non valido");
		}
	}
	
	
	public void translate() throws PigLatinException{
		if(this.isEmpty()) {
			this.setWord(TranslatorConstant.NIL);
		}else {
			if(compositeWord) {
				ArrayList<Word> wordListTmp = new ArrayList<>();
				String[] tmp = this.content.split("-");
				
				StringBuilder wordResult = new StringBuilder();
				
				for(int i = 0; i < tmp.length; ++i) {
					wordListTmp.add(new Word(tmp[i]));
					wordListTmp.get(i).translate();
				}
				
				for(int i = 0; i < wordListTmp.size(); ++i) {
					wordResult.append(wordListTmp.get(i).getWordWithCheckCase());
					
					if(i < wordListTmp.size() - 1) {
						wordResult.append("-");
					}
				}
				
				setWord(wordResult.toString());
			}else {
				translateSingleWord();
			}
		}
	}

	
	public void add(String x){
		
		if(this.finalPunctation) {
			String tmpPunt = getLastChar();

			this.content = this.content.substring(0, this.content.length() - 1) + x + tmpPunt;
		}else {
			this.content += x;
		}
	}
	
	protected void checkCaseProprieties(){
		if(!isEmpty()) {
			upperCaseWord = isUpperCaseWord();
			titleCaseWord = isTitleCaseWord();
			
		}
	}
	
	protected void checkWordExtraProprities(){
		if(!isEmpty()) {
			compositeWord = isCompositeWord();
			finalPunctation = isPunctationWord();
		}
	}
	
	
	protected void initOriginal(){
		if(!isEmpty()) {
			originalLength = content.length() - (isPunctuation(getLastChar()) ? 1 : 0);
			originalString = String.valueOf(content);
		}
	}
	
	protected void translateSingleWord(){
		if(this.startWithVowel()) {
			if(this.endWithY()) {
				this.add(TranslatorConstant.NAY);
			}else if(this.endWithVowel()) {
				this.add(TranslatorConstant.YAY);
			}else if(this.endWithConsonant()) {
				this.add(TranslatorConstant.AY);
			}
		}else if(this.startWithSingleConsonant()) {
			this.moveFirstCharToEnd(TranslatorConstant.AY);
		}else if(this.startWithMoreConsonant()) {
			this.moveFirstConsonatsToEnd(TranslatorConstant.AY);
		}
	}
	
	protected boolean startWithVowel(){
		return isVowel(getFirstChar());	
	}
	
	protected boolean startWithSingleConsonant(){
		return (isConsonant(getFirstChar()) && isVowel(getSecondChar()));
	}
	
	protected boolean startWithMoreConsonant(){
		return (isConsonant(getFirstChar()) && isConsonant(getSecondChar()));
	}
	
	protected boolean endWithConsonant(){
		return finalPunctation ?
				!isVowel(String.valueOf(content.charAt(this.content.length() - 2)))
			:
				!isVowel(getLastChar());

	}
	
	protected boolean endWithVowel(){
		return finalPunctation ? 
				isVowel(String.valueOf(content.charAt(content.length() - 2)))
			:
				isVowel(getLastChar());
	}
	
	protected boolean endWithY(){
		return finalPunctation ?
				String.valueOf(content.charAt(content.length() - 2)).equalsIgnoreCase("y")
			:
				getLastChar().equalsIgnoreCase("y");
	}
	
	public String getWord() {
		return content;
	}
	
	
	public String getWordWithCheckCase(){
		checkCaseProprieties();
				
		if(titleCaseWord) {
			return toTitleCase();
		}else if(upperCaseWord) {
			return content.toUpperCase();
		}else {
			return content;	
		}
	}
	
	public void setWord(String word){
		this.content = word;
	}
	
	protected String getFirstChar(){
		return String.valueOf(this.content.charAt(0));
	}
	
	protected String getSecondChar(){
		return String.valueOf(this.content.charAt(1));
	}
	
	protected String getLastChar(){
		return String.valueOf(this.content.charAt(this.content.length() - 1));
	}
	
	protected boolean isVowel(String character){
		return (
				   character.equalsIgnoreCase("a")
				|| character.equalsIgnoreCase("e")
				|| character.equalsIgnoreCase("i")
				|| character.equalsIgnoreCase("o")
				|| character.equalsIgnoreCase("u")
			  );
	}
	
	protected boolean isConsonant(String character){
		return !isVowel(character);
	}
	
	protected boolean isOnlyConsonantWord(String word){
		for(char c : word.toCharArray()) {
			if(isConsonant(String.valueOf(c))) return false;
		}
		
		return true;
	}
	
	protected boolean isPunctuation(String character){
		return (
				   character.equalsIgnoreCase(".")
				|| character.equalsIgnoreCase(":")
				|| character.equalsIgnoreCase(";")
				|| character.equalsIgnoreCase(",")
				|| character.equalsIgnoreCase("?")
				|| character.equalsIgnoreCase("!")
				|| character.equalsIgnoreCase("'")
				|| character.equalsIgnoreCase("(")
				|| character.equalsIgnoreCase(")")
			  );
	}
	
	protected boolean isCompositeWord(){
		for(int i = 0; i < this.content.length(); ++i) {
			if(String.valueOf(this.content.charAt(i)).equals("-")) {
				return true;
			}
		}
		return false;
	}
	
	protected boolean isPunctationWord(){
		return isPunctuation(getLastChar());
	}
	
	protected boolean isUpperCaseWord(){
		int l = this.content.length();
		
		if(finalPunctation) l -= 1;
		
		Pattern pattern = Pattern.compile("[A-Z]+(ay|yay|nay)[.,;:?!'()]{1}");
	    Matcher matcher = pattern.matcher(content);
	    boolean matchFound = matcher.find();
	    
	    if(matchFound) return true;
		
		for(int i = 0; i < l; ++i) {
			if(Character.isLowerCase(this.content.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	protected boolean isTitleCaseWord(){
		int l = this.content.length();		
		if(finalPunctation) l -= 1;
		
		int countUpperCase = 0;
		
		
		for(int i = 0; i < l; ++i) {
			if(Character.isUpperCase(content.charAt(i))) {
				++countUpperCase;
			}
		}
		
		
		return (countUpperCase == 1);
	}
	
	protected boolean isEmpty(){
		return this.content.equals("");
	}
	
	protected void moveFirtsCharToEndShortWord(String suffix){
		String startChar = getFirstChar();
		
		if(upperCaseWord) suffix = suffix.toUpperCase();


		if(finalPunctation) {
			String tmpPunt = getLastChar();
			
			setWord((suffix != null) 
						? String.valueOf(this.content.charAt(1)) + startChar + suffix + tmpPunt
							: String.valueOf(this.content.charAt(1)) + startChar + tmpPunt
					);
		}else {
			setWord((suffix != null) 
					? String.valueOf(this.content.charAt(1)) + startChar + suffix
						: String.valueOf(this.content.charAt(1)) + startChar
				);
		}
	}
	
	protected void moveFirstCharToEnd(String suffix){
		String startChar = getFirstChar();
				
		if(upperCaseWord) suffix = suffix.toUpperCase();
		
		
		if(this.content.length() == 2) {
			moveFirtsCharToEndShortWord(suffix);
		}else {
			
			if(finalPunctation) {
				String tmpPunt = getLastChar();
				
				setWord((suffix != null) 
						? content.substring(1, this.content.length() - 1) + startChar + suffix + tmpPunt
							: content.substring(1, this.content.length() - 1) + startChar + tmpPunt
					);
			}else {
				setWord((suffix != null) 
						? content.substring(1, this.content.length()) + startChar + suffix
							: content.substring(1, this.content.length()) + startChar
					);
			}
		}
	}
	
	protected void moveFirstConsonatsToEndShortWord(String suffix){
		if(upperCaseWord) suffix = suffix.toUpperCase();

		
		if(finalPunctation) {
			String tmpPunctation = getLastChar();
				
			setWord((suffix != null) 
					? content + suffix + tmpPunctation
						: content + tmpPunctation
				);
		}else {
			setWord((suffix != null) 
					? content + suffix
						: String.valueOf(content)
				);
		}
	}
	
	protected Integer getBufferOfConsonant(StringBuilder consonants){
		for(int i = 0; i < this.content.length(); ++i) {
			String tmpChar = String.valueOf(content.charAt(i));
			
			if(isConsonant(tmpChar)) {					
				consonants.append(tmpChar); 
			}else {
				return i;
			}
		}
		return 0;
	}
	
	protected void moveFirstConsonatsToEnd(String suffix){
		if(isOnlyConsonantWord() || this.content.length() == 2) {
			moveFirstConsonatsToEndShortWord(suffix);
		}else {
			StringBuilder consonants = new StringBuilder();
			Integer lastConsonantIndex = getBufferOfConsonant(consonants);
			
			if(finalPunctation) {
				String tmpPunctation = getLastChar();
				
				setWord((suffix != null) 
						? content.substring(lastConsonantIndex, content.length() - 1) + consonants + suffix + tmpPunctation
							: content.substring(lastConsonantIndex, content.length() - 1) + consonants + tmpPunctation
					);
			}else {
				setWord((suffix != null) 
						? content.substring(lastConsonantIndex, content.length()) + consonants + suffix
							: content.substring(lastConsonantIndex, content.length()) + consonants
					);
			}
		}		
	}
	
	protected boolean isOnlyConsonantWord(){
		for(int i = 0; i < this.content.length(); ++i) {
			if(isVowel(String.valueOf(this.content.charAt(i)))) {
				return false;
			}
		}
		return true;
	}
	
	protected String toTitleCase(){
		StringBuilder buffer = new StringBuilder();
		
		this.content = content.toLowerCase();		
		
		buffer.append(String.valueOf(content.charAt(0)).toUpperCase());
		buffer.append(content.substring(1, content.length()).toLowerCase());
				
		return buffer.toString();
	}
	
	protected void toUpperCase(){
		content = content.toUpperCase();		
	}
}
