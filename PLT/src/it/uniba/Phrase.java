package it.uniba;

import java.util.ArrayList;

public class Phrase{
	private boolean singleWordPhrase = false;
	
	private String content = null;
	private ArrayList<Word> listWord = new ArrayList<>();
	
	public Phrase(String phrase) throws PigLatinException{
		setPhrase(phrase);
	}
	
	public Word getWord(int index){
		return listWord.get(index);
	}
	
	public void parsePhrase() throws PigLatinException{
		this.listWord = new ArrayList<>();
		
		if(isSingleWordPhrase()) {
			this.listWord.add(new Word(this.content));
			
			this.singleWordPhrase = true;
		}else {
			
			ArrayList<Word> tmp = splitPhraseBySpace();
			
			
			for(int i = 0; i < tmp.size(); ++i) {
				this.listWord.add( new Word(tmp.get(i).getWord()));
			}
			
			this.singleWordPhrase = false;
		}
	}
	
	public void translate() throws PigLatinException{
		for(int i = 0; i < this.listWord.size(); ++i) {
			this.listWord.get(i).translate();
		}
	}
	
	public String getPhrase(){		
		if(singleWordPhrase) {
			return this.listWord.get(0).getWordWithCheckCase();
		}else {
			StringBuilder bufferString = new StringBuilder();
			
			for(int i = 0; i < this.listWord.size(); ++i) {
				bufferString.append(this.listWord.get(i).getWordWithCheckCase());
				
				if(i < this.listWord.size() - 1) {
					bufferString.append(" ");
				}
			}
			return bufferString.toString();
		}
		
	}
	
	public void setPhrase(String phrase) throws PigLatinException{
		this.content = phrase;
		
		parsePhrase();
	}
	
	
	public boolean equals(Object o){
		return this.content.equals(o);
	}
	
	public int hashCode(){
		return this.content.hashCode();
	}
	
	
	private ArrayList<Word> splitPhraseBySpace() throws PigLatinException{
		ArrayList<Word> listWords = new ArrayList<>();
			
		String[] tmpArray = this.content.split(" ");
		
		for(int i = 0; i < tmpArray.length; ++i) {
			listWords.add(new Word(tmpArray[i]));
		}
		
		return listWords;
	}
	
	public boolean isSingleWordPhrase(){
		for(int i = 0; i < this.content.length(); ++i) {
			if(String.valueOf(this.content.charAt(i)).equals(" ")) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isMultiWordPhrase(){
		return !isSingleWordPhrase();
	}
	
	public boolean isEmpty(){
		return this.content.equals("");
	}
	
}
