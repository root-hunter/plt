package it.uniba;

import static org.junit.Assert.*;

import org.junit.Test;

public class PhraseTest {

	@Test
	public void testGetPhrase() throws PigLatinException{
		String inputPhrase = "hello hello";
		String exceptedPhrase = "hello hello";
		
		Phrase phrase = new Phrase(inputPhrase);
		
		assertTrue(exceptedPhrase.equals(phrase.getPhrase()));
	}
	
	@Test
	public void testSetPhrase() throws PigLatinException{
		String inputPhrase1 = "hello world";
		String inputPhrase2 = "hello moon";
		
		Phrase phrase = new Phrase(inputPhrase1);
		
		phrase.setPhrase(inputPhrase2);
		
		assertEquals(inputPhrase2, phrase.getPhrase());	
	}
	
	@Test
	public void testPhraseShouldBeSingleWord() throws PigLatinException{
		String inputPhrase = "Hello";		
		Phrase phrase = new Phrase(inputPhrase);
		
		assertTrue(phrase.isSingleWordPhrase());	
	}
	
	@Test
	public void testPhraseShouldNotBeSingleWord() throws PigLatinException{
		String inputPhrase = "Hello World";		
		Phrase phrase = new Phrase(inputPhrase);
		
		assertFalse(phrase.isSingleWordPhrase());	
	}
	
	@Test
	public void testParsePhraseSingleWordPhrase() throws PigLatinException{
		String inputPhrase = "Hello";		
		Phrase phrase = new Phrase(inputPhrase);
		
		phrase.parsePhrase();
		
		assertTrue(inputPhrase.equals(phrase.getPhrase()));	
	}
	
	@Test
	public void testParsePhraseMultiWordPhrase() throws PigLatinException{
		String inputPhrase = "Hello World My Name is Antonio";		
		Phrase phrase = new Phrase(inputPhrase);
		
		phrase.parsePhrase();
		
		if(
				phrase.getWord(0).getWord().equals("Hello")
				&& phrase.getWord(1).getWord().equals("World")
				&& phrase.getWord(2).getWord().equals("My")
				&& phrase.getWord(3).getWord().equals("Name")
				&& phrase.getWord(4).getWord().equals("is")				
				) {
			assertTrue(true);
		}else {
			fail();
		}
	}
	
	//TRANSLATE PHRASE TEST
	@Test
	public void testTranslateSimplePhrase() throws PigLatinException{
		String inputPhrase = "Hello Hello";		
		String exceptedPhrase = "Ellohay Ellohay";
		
		Phrase phrase = new Phrase(inputPhrase);
		
		phrase.translate();
		
		assertTrue(exceptedPhrase.equals(phrase.getPhrase()));	
	}
	
	@Test
	public void testTranslateSimpleLongPhrase() throws PigLatinException{
		String inputPhrase = "Kids know what both cats and dogs are from an early age";		
		String exceptedPhrase = "Idskay owknay atwhay othbay atscay anday ogsday areyay omfray anay earlynay ageyay";
		
		Phrase phrase = new Phrase(inputPhrase);
		
		phrase.translate();
		
		assertTrue(exceptedPhrase.equals(phrase.getPhrase()));	
	}
	
	@Test
	public void testTranslatePhraseWithCompositeWord() throws PigLatinException{
		String inputPhrase = "CP-Wasdf Hello!";		
		String exceptedPhrase = "CPAY-Asdfway Ellohay!";
		
		Phrase phrase = new Phrase(inputPhrase);
		
		phrase.translate();
		
		assertTrue(exceptedPhrase.equals(phrase.getPhrase()));	
	}
	
	@Test
	public void testTranslatePhraseWithCompositeWord2() throws PigLatinException{
		String inputPhrase = "I want A milk-shake!";		
		String exceptedPhrase = "Iyay antway Ayay ilkmay-akeshay!";
		
		Phrase phrase = new Phrase(inputPhrase);
		
		phrase.translate();
		
		assertTrue(exceptedPhrase.equals(phrase.getPhrase()));	
	}
	
	
	
}
