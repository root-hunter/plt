package it.uniba;

import org.junit.Test;

public class PigLatinExceptionTest {

	@Test(expected = PigLatinException.class)
	public void translateSingleSpecialCharSholdBeFail() throws PigLatinException {
		String inputPhrase = "!";
		
		Translator translator = new Translator(inputPhrase);
		
		translator.getPhrase();
	}
}
