package it.uniba;

import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({TranslatorTest.class, PhraseTest.class, WordTest.class, PigLatinExceptionTest.class})
@Category(TranslatorTestSuite.class)
public class TranslatorTestSuite {	
	
}
