package it.uniba;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.junit.experimental.categories.Category;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
@Category(TranslatorTest.class)
public class TranslatorTest {
	
	private static void addParameter(String p1, String p2, ArrayList<ArrayList<String>> parametersList){
		ArrayList<String> tmpArray = new ArrayList<String>();
		
		tmpArray.add(p1);
		tmpArray.add(p2);

		parametersList.add(tmpArray);
	}
	
	private static Object[][] fromArrayListToObjectMatrix(ArrayList<ArrayList<String>> parametersList) {
		 Object[][] parameterArray = new Object[parametersList.size()][2];
		 
		 for(int i = 0; i < parametersList.size(); ++i) {
			 parameterArray[i][0] = parametersList.get(i).get(0);
			 parameterArray[i][1] = parametersList.get(i).get(1);
		 }
		 
		 return parameterArray;
	}
	
	 @Parameters(name = "Test {index} : phrase trasleted from |{0}| to |{1}|")   
	 public static Collection<Object[]> data() {
			 ArrayList<ArrayList<String>> tmpArray = new ArrayList<ArrayList<String>>();
			 
			 addParameter("", "nil", tmpArray);
			 addParameter("hello", "ellohay", tmpArray);
			 addParameter("My name is", "Myay amenay isay", tmpArray);
			 addParameter("a random person", "ayay andomray ersonpay", tmpArray);
			 addParameter("View bet help beat entertaining", "Iewvay etbay elphay eatbay entertainingay", tmpArray);
			 addParameter("Creature carriage adheasive discovery flavor!", "Eaturecray arriagecay adheasiveyay iscoveryday avorflay!", tmpArray);
			 addParameter("It is my milk-shake?", "Itay isay myay ilkmay-akeshay?", tmpArray);
			 addParameter("Very-Hyper-Max", "Eryvay-Erhypay-Axmay", tmpArray);
			 addParameter("Aloy!", "Aloynay!", tmpArray);
			 addParameter("apple", "appleyay", tmpArray);
			 addParameter("hello", "ellohay", tmpArray);
			 addParameter("known", "ownknay", tmpArray);
			 addParameter("hello world", "ellohay orldway", tmpArray);
			 addParameter("well-being", "ellway-eingbay", tmpArray);
			 addParameter("hello world!", "ellohay orldway!", tmpArray);
			 addParameter("Hello world-hello", "Ellohay orldway-ellohay", tmpArray);
			 addParameter("a", "ayay", tmpArray);
			 addParameter("Hello!-Hello!", "Ellohay!-Ellohay!", tmpArray);
			 addParameter("Jumbo!-Rambo!-TANGO?", "Umbojay!-Amboray!-ANGOTAY?", tmpArray);

			 Object[][] parameterArray = fromArrayListToObjectMatrix(tmpArray);
			 
			 return Arrays.asList(parameterArray);
	 }
	
	private final String phrase;
	private final String exceptedPhrase;
	
	public TranslatorTest(String phrase, String exceptedPhrase) {
		this.phrase = phrase;
		this.exceptedPhrase = exceptedPhrase;
	}
	
	@Test
	public void testTranslatePhraseSholdBeCorrect() throws PigLatinException{
		Translator translator = new Translator(this.phrase);
		
		assertEquals(this.exceptedPhrase, translator.getPhrase());
	}
	
}
