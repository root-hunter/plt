package it.uniba;

import static org.junit.Assert.*;

import org.junit.Test;

public class WordTest {

	@Test
	public void testSetWord() throws PigLatinException{
		String inputWord = "hello";
		String inputWord2 = "hola";
		
		Word wordObject = new Word(inputWord);
		
		wordObject.setWord(inputWord2);
		assertEquals(inputWord2, wordObject.getWord());
	}
	
	@Test
	public void testGetWord() throws PigLatinException{
		String inputWord = "hello hello";
		Word wordObject = new Word(inputWord);
		
		assertEquals(inputWord, wordObject.getWord());
	}
	
	@Test
	public void testWordStartWithVowel() throws PigLatinException{
		Word word = new Word("Orange");
		
		assertTrue(word.startWithVowel());
	}
	
	@Test
	public void testWordStartWithSingleConsonant() throws PigLatinException{
		Word word = new Word("Banana");
		
		assertTrue(word.startWithSingleConsonant());
	}
	
	@Test
	public void testWordStartWithMoreConsonant() throws PigLatinException{
		Word word = new Word("Tree");
		
		assertTrue(word.startWithMoreConsonant());
	}
	
	@Test
	public void testWordThatEndWithVowel() throws PigLatinException{
		Word word = new Word("Tree");
		
		assertTrue(word.endWithVowel());
	}
	
	@Test
	public void testWordThatEndWithConsonant() throws PigLatinException{
		Word word = new Word("great");
		
		assertTrue(word.endWithConsonant());
	}
	
	@Test
	public void testWordThatEndWithY() throws PigLatinException{
		Word word = new Word("tasty");
		
		assertTrue(word.endWithY());
	}
	
	@Test
	public void testGetFirstChar() throws PigLatinException{
		Word word = new Word("tasty");
		
		assertTrue("t".equals(word.getFirstChar()));
	}
	
	@Test
	public void testGetSecondChar() throws PigLatinException{
		Word word = new Word("tasty");
		
		assertTrue("a".equals(word.getSecondChar()));
	}
	
	@Test
	public void testGetLastChar() throws PigLatinException{
		Word word = new Word("tasty");
		
		assertTrue("y".equals(word.getLastChar()));
	}
	
	@Test
	public void testWordSholdBeComposite() throws PigLatinException{
		Word word = new Word("tasty-backet");
		
		assertTrue(word.isCompositeWord());
	}
	
	@Test
	public void testWordIsEmpty() throws PigLatinException{
		Word word = new Word("");
		
		assertTrue(word.isEmpty());
	}
	
	@Test
	public void testWordIsOnlyConsonant() throws PigLatinException{
		Word word = new Word("wsdfg");
		
		assertTrue(word.isOnlyConsonantWord());
	}
	
	@Test
	public void testMoveFirstCharToEndOfWord() throws PigLatinException{
		Word word = new Word("banana");
		String exceptedWord = "ananab";
		
		word.moveFirstCharToEnd(null);
		
		assertTrue(exceptedWord.equals(word.getWord()));
	}
	
	@Test
	public void testMoveFirstConsonantsToEndOfWord() throws PigLatinException{
		Word word = new Word("Tree");
		String exceptedWord = "eeTr";
		
		word.moveFirstConsonatsToEnd(null);
		
		assertTrue(exceptedWord.equals(word.getWord()));
	}
	
	@Test
	public void testMoveFirstCharToEndOfWordWithSuffix() throws PigLatinException{
		Word word = new Word("banana");
		String exceptedWord = "ananabay";
		
		word.moveFirstCharToEnd(TranslatorConstant.AY);
		
		assertTrue(exceptedWord.equals(word.getWord()));
	}
	
	@Test
	public void testMoveFirstConsonantsToEndOfWordWithSuffix() throws PigLatinException{
		Word word = new Word("Tree");
		String exceptedWord = "eeTr"+TranslatorConstant.AY;
		
		word.moveFirstConsonatsToEnd(TranslatorConstant.AY);
		
		assertTrue(exceptedWord.equals(word.getWord()));
	}
	
	@Test
	public void testMoveFirstCharToEndOfWordWithSuffixAndPoint() throws PigLatinException{
		Word word = new Word("banana.");
		String exceptedWord = "ananab"+TranslatorConstant.AY+".";
		
		word.moveFirstCharToEnd(TranslatorConstant.AY);
		
		assertTrue(exceptedWord.equals(word.getWord()));
	}
	
	@Test
	public void testMoveFirstConsonantsToEndOfWordWithSuffixAndPoint() throws PigLatinException{
		Word word = new Word("Tree.");
		String exceptedWord = "eeTr"+TranslatorConstant.AY+".";
		
		word.moveFirstConsonatsToEnd(TranslatorConstant.AY);
		
		assertTrue(exceptedWord.equals(word.getWord()));
	}
	
	@Test
	public void testWordSholdBeUpperCase() throws PigLatinException{
		Word word = new Word("TREE");
				
		assertTrue(word.isUpperCaseWord());
	}
	
	@Test
	public void testWordSholdBeUpperCase2() throws PigLatinException{
		Word word = new Word("TREE");
			
		assertTrue(word.upperCaseWord);
	}
	
	@Test
	public void testWordSholdNotBeUpperCase() throws PigLatinException{
		Word word = new Word("tree.");
			
		assertFalse(word.isUpperCaseWord());
	}
	
	@Test
	public void testWordSholdBeTitleCase() throws PigLatinException{
		Word word = new Word("Tree.");
		
		assertTrue(word.isTitleCaseWord());
	}
	
	
	
	@Test
	public void testWordSholdNotBeTitleCase() throws PigLatinException{
		Word word = new Word("tree.");
		
		assertFalse(word.isTitleCaseWord());
	}
	
	//TRANSLATE SINGLE WORD TEST
	
	//WORD START WITH VOWEL
	@Test
	public void testTranslateWordStartWithVowelAndFinishWithVowel() throws PigLatinException{
		Word word = new Word("Orange");
		String exceptedWord = "Orange"+TranslatorConstant.YAY;
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	@Test
	public void testTranslateWordStartWithVowelAndFinishWithConsonant() throws PigLatinException{
		Word word = new Word("Amaren");
		String exceptedWord = "Amaren"+TranslatorConstant.AY;
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	@Test
	public void testTranslateWordStartWithVowelAndFinishWithY() throws PigLatinException{
		Word word = new Word("Amareny");
		String exceptedWord = "Amareny"+TranslatorConstant.NAY;
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	//WORD START WITH CONSONANT
	@Test
	public void testTranslateWordStartWithSingleConsonantAndFinishWithVowel() throws PigLatinException{
		Word word = new Word("Banana");
		String exceptedWord = "Ananab"+TranslatorConstant.AY;
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	@Test
	public void testTranslateWordStartWithSingleConsonantAndFinishWithConsonant() throws PigLatinException{
		Word word = new Word("Bananas");
		String exceptedWord = "Ananasb"+TranslatorConstant.AY;
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	@Test
	public void testTranslateWordStartWithMoreConsonantAndFinishWithVowel() throws PigLatinException{
		Word word = new Word("Tree");
		String exceptedWord = "Eetr"+TranslatorConstant.AY;
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	@Test
	public void testTranslateWordStartWithMoreConsonantAndFinishWithConsonant() throws PigLatinException{
		Word word = new Word("Trees");
		String exceptedWord = "Eestray";
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	//COMPOSITE WORD
	
	@Test
	public void testTranslateWordCompositeWord() throws PigLatinException{
		Word word = new Word("Banana-Shake");
		String exceptedWord = "Ananab"+TranslatorConstant.AY+"-Akesh"+TranslatorConstant.AY;
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	@Test
	public void testTranslateWordCompositeWordWithPucntation() throws PigLatinException{
		Word word = new Word("Banana!-Shake!");
		
		String exceptedWord = "Ananab"+TranslatorConstant.AY+"!"+"-"+"Akesh"+TranslatorConstant.AY+"!";
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	@Test
	public void testTranslateLargeCompositeWord() throws PigLatinException{
		Word word = new Word("Chocolate-chip-milkshake-loving!");
		
		
		String exceptedWord = 
				"Ocolatech"+TranslatorConstant.AY
				+"-"+"ipch"+TranslatorConstant.AY+""
				+"-"+"ilkshakem"+TranslatorConstant.AY+""
				+"-"+"ovingl"+TranslatorConstant.AY+"!";
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	@Test
	public void testTranslateWordWithoutVowel() throws PigLatinException{
		Word word = new Word("CP-wsdf");
		
		
		String exceptedWord = "CPAY-wsdfay";
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	@Test
	public void testTranslateWordWithTitleCase() throws PigLatinException{
		Word word = new Word("Holy!");
		
		
		String exceptedWord = "Olyhay!";
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	
	@Test
	public void testTranslateWordWithTitleCase2() throws PigLatinException{
		Word word = new Word("Hello");
		
		
		String exceptedWord = "Ellohay";
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	
	
	@Test
	public void testTranslateWordWithUpperCase() throws PigLatinException{
		Word word = new Word("HELLO");
		
		String exceptedWord = "ELLOHAY";
		
		word.translate();
		
		assertTrue(exceptedWord.equals(word.getWordWithCheckCase()));
	}
	
	
}
